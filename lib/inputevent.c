#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>

#include "libautomation.h"

static void atm_input_cb(EV_P_ ev_io *w, int revents) {
	struct ATM_INPUT *input = (struct ATM_INPUT *) w;
	struct input_event event;

	if (read(w->fd, &event, sizeof(event)) == sizeof(event))
		input->cb(&event);
}

struct ATM_INPUT *atm_input_by_path(const char *path,
				   void (*cb)(struct input_event *)) {
	char *realpath = atm_globdup(path);
	int fd;
	struct ATM_INPUT *input = NULL;

	if (!realpath)
		return NULL;

	if ((fd = open(realpath, O_RDONLY | O_NONBLOCK)) < 0) {
		atm_log("Can't open %s", realpath);
		goto err;
	}

	input = malloc(sizeof(*input));
	input->cb = cb;
	ev_io_init(&input->watcher, atm_input_cb, fd, EV_READ);
	ev_io_start(EV_A_ &input->watcher);

err:
	free(realpath);

	return input;
}
