#include <limits.h>
#include <string.h>
#include <ev.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <stdlib.h>
#include <glob.h>

#include "libautomation.h"

int atm_file_integer(const char *filename, int *v) {
	int fd, ret;
	char buf[256];

	fd = open(filename, O_RDONLY);
	if(fd < 0)
		atm_fail(filename);

	ret = read(fd, buf, 256);
	close(fd);
	if(ret < 0)
		atm_log("read_device(%s): %s", filename, strerror(errno));
	else {
		*v = atm_read_int_or_fail(buf, filename);

		return 0;
	}

	return -1;
}

int atm_file_float(const char *filename, float *v) {
	int fd, ret;
	char buf[256];

	fd = open(filename, O_RDONLY);
	if(fd < 0)
		atm_fail(filename);

	ret = read(fd, buf, 256);
	close(fd);
	if(ret < 0)
		atm_log("read_device(%s): %s", filename, strerror(errno));
	else {
		*v = atm_read_float_or_fail(buf, filename);

		return 0;
	}

	return -1;
}

int atm_ds_trf_filter(struct ATM_DSTRF_FILTER *transf, int *v) {
	int value, ret;

	ret = transf->src.cb(transf->src.private_data, &value);
	if (ret == 0)
		*v = transf->func(transf->data, value);

	return ret;
}

static int atm_dsgrp_dsgrp_cb (struct ATM_DS *ds) {
	struct ATM_DSGRP *dsgrp = ds->src.private_data;

	return dsgrp->policy(dsgrp);
}

int atm_ds_policyhelper(struct ATM_DS *ds) {
	int ret = ds->src.cb(ds->src.private_data, &ds->dest->v);

	if (ret == 0)
		ds->dest->ts = atm_time;

	return ret;
}

int atm_ds_policy_ignore(struct ATM_DSGRP *grp) {
	int ret;

	while (grp->cur < grp->n) {
		ret = atm_ds_policyhelper(&grp->memb[grp->cur]);
		if (ret > 0)
			return ret;
		++grp->cur;
	}

	grp->cur = 0;

	return 0;
}

int atm_ds_policy_retry(struct ATM_DSGRP *grp) {
	int max_retries = (int) grp->policy_data, retries = 0, ret;

	while (grp->cur < grp->n && retries < max_retries) {
		++retries;

		ret = atm_ds_policyhelper(&grp->memb[grp->cur]);
		if (ret > 0)
			return ret;
		if (ret == 0) {
			++grp->cur;
			retries = 0;
		}
	}

	grp->cur = 0;

	return ret;
}

int atm_ds_policy_reset(struct ATM_DSGRP *grp) {
	struct ATM_DSGRP_PD_RESET *d = grp->policy_data;
	int ret;

	if (d->status == -1) {
		d->status = 0;
		d->enable(d->enable_data);

		return d->delay;
	}

	while (grp->cur < grp->n) {
		ret = atm_ds_policyhelper(&grp->memb[grp->cur]);
		if (ret > 0)
			return ret;
		if (ret < 0)
			++d->status;
	}
	grp->cur = 0;

	if (d->status == 0)
		return 0;

	d->status = -1;
	d->disable(d->disable_data);

	return d->delay;
}

struct ATM_DS *_atm_iiosysfs_setup(const char *dev, const char *channel) {
	struct ATM_DS *ds = NULL;
	struct ATM_DSTRF_FILTER *transf;
	struct ATM_FILTER_SCALE_OFFSET *filter;
	glob_t globbuf;
	char buffer[NAME_MAX], *p, *filename;
	int fd, i, size, offset = 0;
	float scale = 1.0;

	if (dev[0] == '/') {
		glob(dev, GLOB_MARK, NULL, &globbuf);
		if (globbuf.gl_pathc = 0)
			goto err_glob;

		size = strlen(*globbuf.gl_pathv);
		i = 0;
	}
	else {
		glob("/sys/bus/iio/iio:device*/name", 0, NULL, &globbuf);
		for (i = 0; i < globbuf.gl_pathc; ++i) {
			fd = open(globbuf.gl_pathv[i], O_RDONLY);
			size = read(fd, buffer, sizeof(buffer));
			close(fd);
			if (!strncmp(buffer, dev, size)) /* TODO: linebreak */
				break;
		}
		if (i == globbuf.gl_pathc)
			goto err_glob;

		size = strlen(globbuf.gl_pathv[i]) - 4;
		globbuf.gl_pathv[i][size] = 0;
	}
	size = sizeof(buffer);
	p = atm_stradd(buffer, globbuf.gl_pathv[i], &size);
	p = atm_stradd(p, channel, &size);
	if (size < 8)
		goto err_glob;

	strcpy(p, "_input");
	glob(buffer, 0, NULL, &globbuf);
	if (globbuf.gl_pathc == 1) {
		filename = strdup(buffer);
	}
	else {
		strcpy(p, "_raw");
		glob(buffer, 0, NULL, &globbuf);
		if (globbuf.gl_pathc == 0)
			goto err_glob;

		filename = strdup(buffer);
		strcpy(p, "_scale");
		atm_file_float(buffer, &scale);
		strcpy(p, "_offset");
		atm_file_integer(buffer, &offset);
	}

	ds = malloc(sizeof(*ds));

	if (offset != 0 || scale != 1.0) {
		filter = malloc(sizeof(*filter));
		filter->scale = scale;
		filter->offset = offset;

		transf = malloc(sizeof(*transf));
		transf->func = atm_filter_scale_offset;
		transf->data = filter;
		atm_ds_set_char(&transf->src, filename, atm_file_integer);

		atm_ds_set_trf_filter(&ds->src, transf);
	}
	else {
		atm_ds_set_char(&ds->src, filename, atm_file_integer);
	}

err_glob:
	globfree(&globbuf);

	return ds;
}

struct ATM_VALUE *
atm_ds_register(struct ATM_DSGRP *grp, const char *url, const char *key) {
	struct ATM_DS *new_entry;
	char buffer[NAME_MAX], *p;
	int i;
	void *private_data, *cb;

	switch (*url) {
	case 'f':
		if (strncmp(url, "file:", 5) == 0) {
			private_data = atm_globdup(&url[5]);
			if (!private_data) {
				atm_log("Can't find %s", url);
				return NULL;
			}

			cb = atm_file_integer;

			break;
		}

		goto default_case;

	case 'i':
		if (strncmp(url, "iiosysfs:", 9) == 0) {
			url += 9;
			p = strchr(url, ':');
			if (!p)
				return NULL;
			*p++ = 0;

			new_entry = _atm_iiosysfs_setup(url, p);
			if (!new_entry)
				return NULL;

			return new_entry->dest = atm_value(key);
		}

		goto default_case;

	case 'l':
		if (strncmp(url, "local:", 6) == 0) {
			if (!atm_shm_stdmem) {
				atm_log("no data for %s", url);
				return NULL;
			}

			return atm_shm_find(atm_shm_stdmem, &url[6]);
		}

		goto default_case;

	case 's':
		if (strncmp(url, "shm:", 4) == 0) {
			p = strchr(&url[4], '/');
			if (!p)
				return NULL;

			i = p - &url[4];
			strncpy(buffer, &url[4], i);
			buffer[i] = 0;

			return atm_shm_get(buffer, p);
		}
		/* Fall through on error */

	default:
default_case:
		atm_log("Unkown pseudo-url schema in: %s", url);

		return NULL;
	}

	/* We actually need to setup a value in shared memory */

	return atm_dsgrp_insert(grp, cb, private_data, atm_value(key))->dest;
}

struct ATM_DS *atm_dsgrp_insert(struct ATM_DSGRP *grp, void *cb, void *pdata,
				struct ATM_VALUE *dest) {

	if (grp->n >= grp->size) {
		grp->size <<= 1;
		grp->memb = realloc(grp->memb, grp->size * sizeof(*grp->memb));
	}

	grp->memb[grp->n].src.cb = cb;
	grp->memb[grp->n].src.private_data = pdata;
	grp->memb[grp->n].dest = dest;

	return &grp->memb[grp->n++];
}

struct ATM_VALUE *atm_ds(const char *url, const char *key) {
	return atm_ds_register(&atm_main_task.dsgrp, url, key);
}

struct ATM_DSGRP *atm_dsgrp_dsgrp(struct ATM_DSGRP *grp) {
	static struct ATM_VALUE dummy_dest;
	struct ATM_DSGRP *newgrp = malloc(sizeof(*newgrp));

	atm_dsgrp_init(newgrp);
	atm_dsgrp_insert(grp, atm_dsgrp_dsgrp_cb, newgrp, &dummy_dest);
}

void atm_dsgrp_init(struct ATM_DSGRP *grp) {
	grp->cur = grp->n = 0;
	grp->size = 4;
	grp->memb = malloc(grp->size * sizeof(*grp->memb));
	grp->policy_data = NULL,
	grp->policy = atm_ds_policy_ignore;
}
