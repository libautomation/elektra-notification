#ifndef _LIBAUTOMATION_H
#define _LIBAUTOMATION_H

#include <ev.h>
#include <linux/input.h>

/*
 * Generally useful functions
 */
/* TODO: Should this follow syslog() or do_log() ... */
extern void atm_log(const char *fmt, ...);
extern void atm_fail(const char *message);
extern int atm_echo(const char *str, const char *filename);
extern int atm_timestamp(void);
extern long int atm_read_int_or_fail(const char *str, const char *errmsg);
extern float atm_read_float_or_fail(const char *str, const char *msg);
extern char *atm_stradd(char *dest, const char *source, int *size);
extern void atm_call_in(ev_tstamp seconds, void *func_pointer);

/* return NULL if pattern doesn't match any file */
extern char *atm_globdup(const char *pattern);

extern void atm_do_nothing(void);

/* return 0 if value was updated successfully */
extern int atm_file_float(const char *filename, float *value);
extern int atm_file_integer(const char *filename, int *value);

/* start the event loop, probably doesn't return */
extern int atm_main(int argc, char **argv);

extern int atm_time;
extern struct ev_loop *loop;
extern struct ATM_TASK atm_main_task;
#define atm_main_task_timer ((ev_timer *) &atm_main_task)

typedef volatile int atm_shm_t;

struct ATM_VALUE {
	atm_shm_t v;	/* the value */
	atm_shm_t ts;	/* last update */
};

/* Create a new variable in standard shared memory area or on the heap
   if shared memory is not available or key == NULL.*/
extern struct ATM_VALUE *atm_value(const char *key);

struct ATM_SHM_HEADER {
	int ks;
	int value_count;
	unsigned char magic;
};

/* This is supposed to be an opaque data structure. We might change it
   without notice. So typedef it. */
typedef struct {
	struct ATM_SHM_HEADER *header;
	char *keys;
	struct ATM_VALUE *data;
	int fd;
} *ATM_SHM;

/*
 * Shared memory interface - client side
 */

/* Low-level API - mostly for internal use */

extern ATM_SHM atm_shm_attach(const char *id);
extern struct ATM_VALUE *atm_shm_find(ATM_SHM mem, const char *key);

/* High-level API */

/* returns NULL if something goes wrong.
   Maybe have some variants of this: blocking, crashing, ... */
extern struct ATM_VALUE *atm_shm_get(const char *id, const char *key);

/*
 * Shared memor interface - server side
 */

extern ATM_SHM atm_shm_stdmem; /* Set to first shared memory created */
extern ATM_SHM atm_shm_create(const char *id);
extern struct ATM_VALUE *atm_shm_register(ATM_SHM mem, const char *key);

/* Convenience function to update timestamp automatically */
extern void atm_shm_update(struct ATM_VALUE *var, int value);

/*
 * Filters (data processing)
 */

struct ATM_FILTER_SCALE_OFFSET {
	float scale;
	int offset;
};

struct ATM_FILTER_MOVING_AVERAGE {
	int length;
	int pos;
	int *data;
	int value;
};

extern int atm_filter_scale_offset(void *f_scale_offset, int value);
extern int atm_filter_moving_average(void *f_mov_average, int val);
extern struct ATM_FILTER_MOVING_AVERAGE *atm_filter_moving_average_create
						(int length, int init_value);

/*
 * Date source (shm, iio, file, ...) interface
 */

struct ATM_DSGRP_PD_RESET {
	int delay;
	int status;
	void (*disable) (void *);
	void (*enable) (void *);
	void *disable_data;
	void *enable_data;
};

struct ATM_DS_BARE {
	int (*cb) (void *, atm_shm_t *);
	void *private_data;
};

struct ATM_DS {
	struct ATM_DS_BARE src;
	struct ATM_VALUE *dest;
};

struct ATM_DSGRP {
	struct ATM_DS *memb;
	int n;
	int cur;
	int size;

	/* This function returns:
	 *  0: nominal operation
	 * >0: request to restart after ret milli seconds
	 * <0: error condition
	 */
	int (*policy) (struct ATM_DSGRP *);
	void *policy_data;
};

extern void atm_dsgrp_init(struct ATM_DSGRP *grp);
extern struct ATM_DSGRP *atm_dsgrp_dsgrp(struct ATM_DSGRP *grp);
extern struct ATM_DS *atm_dsgrp_insert(struct ATM_DSGRP *grp, void *cb,
				       void *pdata, struct ATM_VALUE *dest);

extern int atm_ds_policy_ignore(struct ATM_DSGRP *grp);
extern int atm_ds_policy_reset(struct ATM_DSGRP *grp);
extern int atm_ds_policy_retry(struct ATM_DSGRP *grp);

extern struct ATM_VALUE *
atm_ds_register(struct ATM_DSGRP *grp, const char *url, const char *key);
extern struct ATM_VALUE *atm_ds(const char *url, const char *key);


struct ATM_DSTRF_FILTER {
	struct ATM_DS_BARE src;
	void *data;
	int (*func)(void *, int);
};

extern int atm_ds_trf_filter(struct ATM_DSTRF_FILTER *transform, int *v);

static inline void atm_ds_set_char(struct ATM_DS_BARE *ds, const char *pdata,
				   int (*cb) (const char *, int *)) {
	ds->private_data = (void *) pdata;
	ds->cb = (void *) cb;
}

static inline void atm_ds_set_trf_filter(struct ATM_DS_BARE *ds,
					 struct ATM_DSTRF_FILTER *transform) {
	ds->private_data = (void *) transform;
	ds->cb = (void *) atm_ds_trf_filter;
}

/*
 * Tasks
 */

#define ATM_TIMER_RES 0.001

struct ATM_TASK {
	ev_timer watcher;
	struct ATM_DSGRP dsgrp;
	void (*task) (void);
};

extern void atm_task_init(struct ATM_TASK *task, ev_tstamp interval);

/*
 * (Linux) Input devices - not quite clear how (if) this can be generalized
 * to other types of input events - like lcdproc key presses and menu events.
 */

struct ATM_INPUT {
	ev_io watcher;
	void (*cb) (struct input_event *e);
};

/* return NULL if pattern can't be opened for reading, log errormessage */
extern struct ATM_INPUT *atm_input_by_path(const char *path_pattern,
					  void (*cb)(struct input_event *));

#endif
