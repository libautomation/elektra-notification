/*
This file contains:
 * initialization code
 * important globals
 * some utility functions used throughout the library
*/

#include "libautomation.h"

struct ATM_TASK atm_main_task;
struct ev_loop *loop;
int atm_time;

void __attribute__ ((constructor)) atm_init(void) {
	loop = EV_DEFAULT;
	atm_task_init(&atm_main_task, 60);
}

int atm_main(int argc, char **argv) {

	ev_timer_start(EV_A_ (ev_timer *) &atm_main_task);

	ev_run(EV_A_ 0);
}

void atm_do_nothing(void) {
}
