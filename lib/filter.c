#include <stdlib.h>

#include "libautomation.h"

int atm_filter_scale_offset(void *filter_data, int v) {
	struct ATM_FILTER_SCALE_OFFSET *f = filter_data;

	return v * f->scale + f->offset;
}

int atm_filter_moving_average(void *f_mov_average, int val) {
	struct ATM_FILTER_MOVING_AVERAGE *filter = f_mov_average;

	filter->value -= filter->data[filter->pos];
	filter->data[filter->pos] = val;
	if (++filter->pos >= filter->length)
		filter->pos = 0;

	return (filter->value += val) / filter->length;
}

struct ATM_FILTER_MOVING_AVERAGE *atm_filter_moving_average_create
						(int length, int init_value) {
	struct ATM_FILTER_MOVING_AVERAGE *f =
				malloc(sizeof(*f) + length * sizeof(int *));
	int i;

	f->length = length;
	f->pos = 0;
	f->data = (int *) f + sizeof(*f);
	f->value = init_value * length;

	for(i = 0; i < length; ++i)
		f->data[i] = init_value;

	return f;
}
