#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <glob.h>

#include "libautomation.h"

char *atm_stradd(char *dest, const char *source, int *size) {
	int len;

	len = strlen(source);
	if (len >= *size)
		len = *size - 1;
	memcpy(dest, source, len);
	dest[len] = 0;
	*size -= len;

	return dest + len;
}

int atm_echo(const char *str, const char *filename) {
	int fd, ret;

	fd = open(filename, O_WRONLY);
	if(fd < 0)
		return fd;

	ret = write(fd, str, strlen(str));
	close(fd);

	return ret;
}

void atm_log(const char *fmt, ...) {
	va_list ap;

	va_start(ap, fmt);
	vfprintf(stderr, fmt, ap);
	va_end(ap);
}

void atm_fail(const char *message) {
	perror(message);
	exit(1);
}

long int atm_read_int_or_fail(const char *str, const char *msg){
	char *s;
	long int res;

	res = strtol(str, &s, 10);
	if(s == str)
		atm_fail(msg);

	return res;
}

float atm_read_float_or_fail(const char *str, const char *msg){
	char *s;
	float res;

	res = strtod(str, &s);
	if(s == str)
		atm_fail(msg);

	return res;
}


int atm_timestamp(void) {
	struct timespec t;

	clock_gettime(CLOCK_BOOTTIME, &t);

	return t.tv_sec*10 + t.tv_nsec/100000000;
}

/* Find a uniq filename to a glob pattern,
 * useful for ever changing sysfs paths
 */
char *atm_globdup(const char *pattern) {
	glob_t globbuf;
	char *result;

	glob(pattern, 0, NULL, &globbuf);
	if(globbuf.gl_pathc == 0) {
		atm_log("WARNING: Can't match pattern '%s'", pattern);
		result = NULL;
	}
	else {
		result = strdup(*globbuf.gl_pathv);
		if(globbuf.gl_pathc > 1)
			atm_log("WARNING: Pattern '%s' matches multiple files. Using '%s'", pattern, result);
	}

	globfree(&globbuf);

	return result;
}
