#include "libautomation.h"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>        /* For mode constants */
#include <fcntl.h>           /* For O_* constants */
#include <sys/file.h>
#include <limits.h>

#include "managed_data.h"

#define MAGIC_NUMBER 217

#define SHM_SIZE 4096

ATM_SHM atm_shm_stdmem;

static int _atm_shm_open(const char *id, int flags) {
	char name[NAME_MAX];

	snprintf(name, NAME_MAX, "/dev/shm/libautomation_%s", id);
	return open(name, flags, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
}

static ATM_SHM _atm_shm_alloc(struct ATM_SHM_HEADER *h) {
	ATM_SHM mem = malloc(sizeof(*mem));

	mem->header = h;
	mem->keys = (void *) h + sizeof(*h);
	mem->data = (void *) h + SHM_SIZE/2;

	return mem;
}


ATM_SHM atm_shm_attach(const char *id) {
	struct ATM_SHM_HEADER *h;
	ATM_SHM mem;
	int fd;

	fd = _atm_shm_open(id, O_RDONLY);
	if (fd < 0)
		return NULL;

	flock(fd, LOCK_EX);

	h = mmap(NULL, SHM_SIZE, PROT_READ, MAP_SHARED, fd, 0);
	if (h == MAP_FAILED)
		atm_fail(id);  /* Should never happen */

	if (h->magic != MAGIC_NUMBER) {
		atm_log("Shared memory with id '%s' is corrupted!", id);
		exit(1);
	}

	mem = _atm_shm_alloc(h);
	flock(fd, LOCK_UN);

	return mem;
}

ATM_SHM atm_shm_create(const char *id) {
	struct ATM_SHM_HEADER *h;
	ATM_SHM mem;
	int fd;
	struct stat fd_info;

	fd = _atm_shm_open(id, O_RDWR | O_CREAT);
	if (fd < 0)
		return NULL;

	flock(fd, LOCK_EX);
	fstat(fd, &fd_info);
	if (fd_info.st_size == 0)  /* File was just created */
		ftruncate(fd, SHM_SIZE);
	else if (fd_info.st_size != SHM_SIZE)  /* Not our file !!! */
		goto wrong_file;

	h = mmap(NULL, SHM_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
	if (h == MAP_FAILED)
		atm_fail(id);  /* Should never happen */

	mem = _atm_shm_alloc(h);
	mem->fd = fd;

	if (fd_info.st_size == 0) {	/* Need to initialize data structure */
		/* mem->keys[] already initialized by ftruncate() */
		h->magic = MAGIC_NUMBER;
	}
	else if (h->magic != MAGIC_NUMBER) {
wrong_file:
		atm_log("Shared memory with id '%s' is corrupted!", id);
		exit(1);
	}

	flock(fd, LOCK_UN);

	if (!atm_shm_stdmem)
		atm_shm_stdmem = mem;

	return mem;
}

/*
 * Internal version of atm_shm_find() assumes that mem->keys[] is already
 * locked by mem->header->sem ...
 */
static int _atm_shm_find(ATM_SHM mem, const char *key) {
	int i;
	char *p = mem->keys;

	for (i = 0; *p; ++i) {
		if (!strcmp(p, key))
			return i;

		p += strlen(&mem->keys[i]) + 1;
	}

	return -1;
}

struct ATM_VALUE *atm_shm_find(ATM_SHM mem, const char *key) {
	int i;

	flock(mem->fd, LOCK_EX);
	i = _atm_shm_find(mem, key);
	flock(mem->fd, LOCK_UN);

	return i == -1 ? NULL : &mem->data[i];
}

struct ATM_VALUE *atm_shm_register(ATM_SHM mem, const char *key) {
	int i, keysize;

	flock(mem->fd, LOCK_EX);

	i = _atm_shm_find(mem, key);
	if (i == -1) {
		keysize = strlen(key) + 1;
		if (keysize + mem->header->ks + sizeof(*mem->header) >= SHM_SIZE/2) {
			atm_log("running out of shared memory, crashing ...");
			exit(1);
		}
		strcpy(&mem->keys[mem->header->ks], key);
		mem->header->ks += keysize;
		i = mem->header->value_count++;
	}

	flock(mem->fd, LOCK_UN);

	return &mem->data[i];
}

struct MD_LUT _atm_shm_lut = {0, 0, NULL};

struct ATM_VALUE *atm_shm_get(const char *id, const char *key) {
	struct MD_LUT_ENTRY *e = NULL;
	ATM_SHM mem;

	if (_atm_shm_lut.size == 0)
		md_lut_init(&_atm_shm_lut);
	else
		e = md_lut_find(&_atm_shm_lut, id);

	if (e)
		return atm_shm_find(e->data, key);

	mem = atm_shm_attach(id);
	if (!mem)
		return NULL;

	md_lut_insert(&_atm_shm_lut, id, mem);

	return atm_shm_find(mem, key);
}

void atm_shm_update(struct ATM_VALUE *var, int value) {
	var->v = value;
	var->ts = atm_timestamp();
}

struct ATM_VALUE *atm_value(const char *key) {
	return (atm_shm_stdmem && key) ? atm_shm_register(atm_shm_stdmem, key)
				       : malloc(sizeof(struct ATM_VALUE));
}
