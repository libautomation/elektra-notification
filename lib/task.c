#include "libautomation.h"

void atm_call_in(ev_tstamp seconds, void *f) {
	ev_once(EV_A_ -1, 0, seconds, f, NULL);
}

void atm_task_cb(EV_P_ ev_timer *w, int revents) {
	struct ATM_TASK *task = (struct ATM_TASK *) w;
	ev_tstamp delay;
	int ret;

	atm_time = atm_timestamp();

	ret = task->dsgrp.policy(&task->dsgrp);
	if (ret > 0) {
		ev_timer_stop(EV_A_ w);
		ev_timer_set(w, ret * ATM_TIMER_RES, w->repeat);
		ev_timer_start(EV_A_ w);

		return;
	}

	task->task();
}

void atm_task_init(struct ATM_TASK *task, ev_tstamp interval) {
	ev_timer_init(&task->watcher, atm_task_cb, 0.1, interval);
	atm_dsgrp_init(&task->dsgrp);
	task->task = atm_do_nothing;
}
