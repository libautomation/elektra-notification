#ifndef _MANAGEDDATA_H
#define _MANAGEDDATA_H

#include <search.h>
#include <stdlib.h>
#include <string.h>

/* Collection of primitive data structures that for some reason are still
 * not available as C library. Since this is only a header file, all functions
 * have to be defined as inline.
 */

#ifndef MD_DECL
#define MD_DECL static inline
#endif

struct MD_LUT_ENTRY {
	const char *key; /* Needs to be first, so that strcmp() works */
	void *data;
};

struct MD_LUT {
	size_t n;
	size_t size;
	struct MD_LUT_ENTRY *start;
};

/*
MD_DECL int _md_lut_compare(struct MD_LUT_ENTRY *k1, struct MD_LUT_ENTRY *k2) {
	return strcmp(k1->key, k2->key);
}
*/

MD_DECL struct MD_LUT_ENTRY *md_lut_find(struct MD_LUT *t, const char *key) {
	struct MD_LUT_ENTRY ref = {key, NULL};

	return lfind(&ref, t->start, &t->n, sizeof(struct MD_LUT_ENTRY),
		     (int (*)(const void *, const void *)) strcmp);
}

MD_DECL void md_lut_insert(struct MD_LUT *t, const char *key, void *data) {
	if (t->n == t->size) {
		t->size <<= 1;
		t->start = realloc(t->start, t->size * sizeof(*t->start));
	}

	t->start[t->n].key = key;
	t->start[t->n].data = data;
	t->n++;
}

MD_DECL void md_lut_init(struct MD_LUT *t) {
	t->size = 8;
	t->n = 0;
	t->start = malloc(8 * sizeof(*t->start));
}

#endif
