#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <ev.h>

#include "../lib/libautomation.h"

static void enable(void *filename) {
	atm_echo("1", (char *) filename);
}

static void disable(void *filename) {
	atm_echo("0", (char *) filename);
}

static struct ATM_DSGRP_PD_RESET policy_reset_template = {
	.delay = 2000,
	.status = 0,
	.disable = disable,
	.enable = enable,
};

static void parse_file(const char *name, struct ATM_DSGRP *grp) {
	FILE *f;
	char *line = NULL, *first, *rest;
	int len;

	f = fopen(name, "r");
	if (!f)
		atm_fail(name);

	while (getline(&line, &len, f) > 0) {
		if(line[0] == '#')
			continue;

		first = strtok(line, " \t");
		rest = strtok(NULL, "\n");

		if (!strcmp(first, "dsgrp")) {
			parse_file(rest, atm_dsgrp_dsgrp(grp));
		}
		else if (!strcmp(first, "reset")) {
			grp->policy = atm_ds_policy_reset;

			/* Memory leaks here: */
			policy_reset_template.enable_data = strdup(rest);
			policy_reset_template.disable_data =
				policy_reset_template.enable_data;

			grp->policy_data = realloc(grp->policy_data,
						sizeof(policy_reset_template));
			memcpy(grp->policy_data, &policy_reset_template,
			       sizeof(policy_reset_template));
		}
		else if (!strcmp(first, "retry")) {
			grp->policy = atm_ds_policy_retry;
			grp->policy_data = realloc(grp->policy_data,
						   sizeof(int));

			*(int*)grp->policy_data = atm_read_int_or_fail(rest, first);
		}
		else if (!strcmp(first, "interval")) {
			(*(ev_timer *) &atm_main_task).repeat =
				atm_read_int_or_fail(rest, first);
		}
		else
			atm_ds_register(grp, rest, first);
	}

	free(line);
	fclose(f);
}

int main(int argc, char **argv) {
	const char *p;

	if(!argv[1])
		atm_fail("atmd called without argument - exiting");

	p = strrchr(argv[1], '/'); /* Get the filename without path */
	if (!p)
		p = argv[1];
	else
		++p;
	atm_shm_create(p);

	parse_file(argv[1], &atm_main_task.dsgrp);

	return atm_main(argc, argv);
}
