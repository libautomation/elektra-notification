#include <string.h>
#include <stdio.h>

#include "../lib/libautomation.h"

int main(int argc, char **argv) {
	ATM_SHM mem;
	const char *p;
	int i;

	if (!argv[1])
		atm_fail("atmdump called without argument - exiting");

	mem = atm_shm_attach(argv[1]);
	if (!mem)
		atm_fail("atmdump can't attach shared memory");

	for (i = 0, p = mem->keys; *p; ++i, p += strlen(p))
		printf("%d: '%s' - last update: %d, value %d\n", i, p,
		       mem->data[i].ts, mem->data[i].v);

	return 0;
}
