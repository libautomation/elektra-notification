#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <ev.h>

#include "../lib/libautomation.h"

ev_io input_watcher;
ev_timer ack_watcher;

/* Global configuration variables set in read_config() */
char *ack_pipe = "/dev/line_monitor", *hints = NULL, *line = NULL;
char *alarm_command = "gsmalarm";
ev_tstamp ack_timeout = 600;
int verbose = 0, polarity = 1;

/* Internal state */
enum State {
	boot,
	active,
	inactive,
	ack
} state = boot;

int check_line(const char *filename){
	int count = 0, val, last_val;

	/* require at least 3 consecutive reads of the same value to
	 * suppress noise.
	 */
	for(count = 0; count < 3; ++count){
		atm_file_integer(filename, &val);
		val = val ? 1 : 0;
		if(val != last_val){
			last_val = val;
			count = 0;
		}
	}

	return val;
}

void main_task(void){
	if(check_line(line) == polarity){
		switch(state) {
		case inactive:
			ev_timer_stop(EV_A_ &ack_watcher);
			break;
		case ack:
			if(hints)
				atm_echo("1", hints);
			break;
		default:
			break;
		}
		state = active;
	}
	else if (state == active) {
		ev_timer_set(&ack_watcher, 0.01, ack_timeout);
		ev_timer_start(EV_A_ &ack_watcher);
		state = inactive;
	}

	if(verbose)
		printf("state: %d\n", state);
}

void read_config(const char *filename){
        int i;
        char *line = NULL, *first, *rest;
        size_t len = 0;
        FILE *f;

        f = fopen(filename, "r");
        if(!f)
                atm_fail(filename);

        while(getline(&line, &len, f) > 0){
                if(line[0] == '#')
                        continue;

                first = strtok(line, " \t");
                rest = strtok(NULL, "\n");

		if(!strcmp(first, "line"))
			line = atm_globdup(rest);
		else if(!strcmp(first, "hints"))
			hints = strdup(rest);
		else if(!strcmp(first, "ack_pipe"))
			ack_pipe = strdup(rest);
		else if(!strcmp(first, "command"))
			alarm_command = strdup(rest);
		else if(!strcmp(first, "timeout"))
			ack_timeout = atm_read_float_or_fail(rest, first);
                else if(!strcmp(first, "interval"))
                        atm_main_task_timer->repeat =
					atm_read_float_or_fail(rest, first);
		else if(!strcmp(first, "polarity"))
			polarity = atm_read_int_or_fail(rest, first);
		else
			atm_log("ignoring unknown config option '%s'", first);
	}

	free(line);
	fclose(f);
}

static void input_cb(EV_P_ ev_io *w, int revents){
	char buffer[1024];

	if(read(w->fd, buffer, 1024) > 0 && state == inactive){
		state = ack;
		if(hints)
			atm_echo("0", hints);
		ev_timer_stop(EV_A_ &ack_watcher);
	}
}

static void trigger_alarm(EV_P_ ev_timer *w, int revents){
	system(alarm_command);
}

int main(int argc, char **argv){
	char *configfile;
	int fd, i;

	if(!argv[1])
		configfile = "/etc/line_monitor.conf";
	else {
		configfile = argv[1];
		if(argv[2])
			verbose = 1;
	}

	read_config(configfile);

	atm_log("start");

	if((fd = open(ack_pipe, O_NONBLOCK | O_RDONLY)) < 0)
		atm_fail(ack_pipe);

	/* Avoid getting EOF on the fifo */
	open(ack_pipe, O_NONBLOCK | O_WRONLY);

	ev_io_init(&input_watcher, input_cb, fd, EV_READ);
	ev_io_start(EV_A_ &input_watcher);

	if(hints){
		i = 1; /* Cause an alarm if we can't read hints */
		atm_file_integer(hints, &i);
		if(i == 0)
			state = ack;
	}

	ev_init(&ack_watcher, trigger_alarm);
	atm_main_task.task = main_task;

	return atm_main(argc, argv);
}
