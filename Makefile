CC ?= cc

all:
	make -C lib
	make -C examples
	make -C tools
